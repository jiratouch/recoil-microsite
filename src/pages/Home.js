import React from 'react';
import { liffProfile } from '../global/selectors';
import { useRecoilValue } from 'recoil';
import { Card, Row } from 'antd';

const { Meta } = Card;

const Home = () => {
  const profile = useRecoilValue(liffProfile);

  return (
    <Row justify="center">
      <Card
      hoverable
      style={{ width: 240 }}
      cover={<img alt="example" src={profile.pictureUrl}/>}
      >
        <Meta title={profile.displayName} description={profile.email} />
      </Card>
    </Row>
  )
};

export default Home;