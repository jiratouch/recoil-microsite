import React from 'react'
import { Switch, Route } from 'react-router'
import { Layout, Row, Col } from 'antd'
import Home from '../pages/Home'

const Authenticated = () => {
  return (
      <Layout className="layout">
        <Layout.Header>
          <Row justify="center">
            <Col>
              <div className="logo">Doosoft</div>
            </Col>
          </Row>
        </Layout.Header>
        <Layout.Content className="content">
          <Switch>
            <Route path="/" exact>
                <Home />
            </Route>
          </Switch>
        </Layout.Content>
        <Layout.Footer style={{ textAlign: 'center' }}>© 2020 Doosoft. All Rights Reserved.</Layout.Footer>
      </Layout>
  )
}

export default Authenticated
