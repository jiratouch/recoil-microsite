import { atom } from 'recoil';

export const liffId = atom({
    key: 'liffId',
    default: window._env.LIFFID || '1654650267-7aZ1OKWQ',
});