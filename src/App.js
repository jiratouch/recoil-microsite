import React, { Suspense } from 'react';
import { RecoilRoot } from 'recoil';
import Authenticated from '../src/layouts/Authenticated'
import './App.less';
import { BrowserRouter } from 'react-router-dom';
import { Switch, Route } from 'react-router';
import { Skeleton } from 'antd'
import qs from 'qs'
import vConsole from 'vconsole'

function App() {
  const query = qs.parse(window.location.search, {ignoreQueryPrefix: true})
  if (process.env.NODE_ENV === 'development' || query.dev) {
    new vConsole();
  }
  return (
    <RecoilRoot>
      <BrowserRouter>
        <Switch>
          <Route>
            <Suspense fallback={<Skeleton active />}>
              <Authenticated />
            </Suspense>
          </Route>
        </Switch>
      </BrowserRouter>
    </RecoilRoot>
  );
}

export default App;
