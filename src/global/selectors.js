import { selector } from 'recoil';
import { liffId } from '../global/atoms' 
import liff from '@line/liff'


//Get Profile from Line Fronend Framework
export const liffProfile = selector({
    key: 'liffProfile',
    get: async ({get}) => {
      const id = get(liffId);
  
      if (!id) {
        return null;
      }
      try {

        await liff.init({ liffId: id })

        if (!liff.isLoggedIn()) {
            liff.login({
            redirectUri: `${window.location.protocol}//${window.location.host}?next=${window.location.pathname}`
            })
        }
        const profile = await liff.getProfile()
        const email = liff.getDecodedIDToken().email;
        const data = {...profile,email}

        return data;
      }
      catch (e) {
        console.debug('Init liff error', e)
        return Promise.reject(e)
      }
    },
  });